<?php

namespace App\Entity;

use App\Repository\BookRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass=BookRepository::class)
 * @ORM\Table(name="`books`")
 */
class Book
{
    /**
     * @ORM\Id()
     * @ORM\GeneratedValue()
     * @ORM\Column(type="integer")
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $title;

    /**
     * @ORM\Column(type="float")
     */
    private $price;

    /**
     * @ORM\Column(type="string", length=255)
     */
    private $author;

    /**
     * @ORM\OneToMany(targetEntity=OrderBook::class, mappedBy="book")
     */
    private $bookId;

    /**
     * @ORM\ManyToOne(targetEntity=Category::class, inversedBy="categoryId")
     * @ORM\JoinColumn(nullable=false)
     */
    private $category;

    public function __construct()
    {
        $this->bookId = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getPrice(): ?float
    {
        return $this->price;
    }

    public function setPrice(float $price): self
    {
        $this->price = $price;

        return $this;
    }

    public function getAuthor(): ?string
    {
        return $this->author;
    }

    public function setAuthor(string $author): self
    {
        $this->author = $author;

        return $this;
    }

    /**
     * @return Collection|OrderBook[]
     */
    public function getBookId(): Collection
    {
        return $this->bookId;
    }

    public function addBookId(OrderBook $bookId): self
    {
        if (!$this->bookId->contains($bookId)) {
            $this->bookId[] = $bookId;
            $bookId->setBook($this);
        }

        return $this;
    }

    public function removeBookId(OrderBook $bookId): self
    {
        if ($this->bookId->contains($bookId)) {
            $this->bookId->removeElement($bookId);
            // set the owning side to null (unless already changed)
            if ($bookId->getBook() === $this) {
                $bookId->setBook(null);
            }
        }

        return $this;
    }

    public function getCategory(): ?Category
    {
        return $this->category;
    }

    public function setCategory(?Category $category): self
    {
        $this->category = $category;

        return $this;
    }
}
