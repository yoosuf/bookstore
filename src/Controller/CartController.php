<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Order;
use App\Entity\OrderBook;
use App\Entity\User;
use App\Utils\Cart;
use App\Utils\CartItem;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Session\Attribute\NamespacedAttributeBag;
use Symfony\Component\HttpFoundation\Session\Session;
use Symfony\Component\HttpFoundation\Session\Storage\NativeSessionStorage;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;

class CartController extends AbstractController
{


    protected $session;

    protected $cart;


    public function __construct()
    {
        $storage = new NativeSessionStorage();
        $attributes = new NamespacedAttributeBag();
        $this->session = new Session($storage, $attributes);
        $this->cart = new Cart($this->session);
    }

    /**
     * Takes the user to the cart list
     * @Route("/cart", name="cart_index")
     */
    public function showCartAction()
    {
        $cart = $this->cart->getItems();

        return $this->render('cart/index.html.twig', ['cart' => $cart]);
    }

    /**
     * Clears the cart
     *
     * @Route("/cart/clear", name="cart_clear")
     */
    public function clearCartAction()
    {
        $this->cart->clear();

        $this->addFlash('success', 'Cart cleared');

        return $this->redirectToRoute('home');
    }

    /**
     * Adds coupon to the cart
     *
     * @Route("/cart/add-coupon", name="coupon_add")
     */
    public function addCouponAction(Request $request)
    {
        $coupon = $request->get('coupon', null);

        if (! empty($coupon)) {
            $this->cart->setCoupon($coupon);
            $this->addFlash('success', 'Coupon redeemed successfully.');
        } else {
            $this->addFlash('danger', 'Coupon code cannot be empty.');
        }

        return $this->redirectToRoute('cart_index');
    }

    /**
     *  Adds the book to cart list
     *
     * @Route("/cart/{id}", name="cart_add", requirements={"id": "\d+"}, methods="GET")
     */
    public function addToCartAction(Book $book)
    {
        $item = new CartItem([
            'id' => $book->getId(),
            'name' => $book->getTitle(),
            'price' => $book->getPrice(),
        ]);


        $item->setQuantity(1);
        $item->setCategoryId($book->getCategory()->getId());
        $this->cart->addItem($item);

        $this->addFlash('success', 'Book added to cart successfully.');

        return $this->redirectToRoute('cart_index');
    }

    /**
     * Removes given book from the cart
     *
     * @Route("/cart/remove/{id}", name="cart_remove", requirements={"id": "\d+"})
     */
    public function removeCartAction(int $id)
    {
        $this->cart->removeItem($id);
        $this->addFlash('success', 'Book removed from the cart.');

        return $this->redirectToRoute('home');
    }

    /**
     * Checkout process of the cart
     *
     * @Route("/cart/checkout", name="cart_checkout")
     */
    public function checkOutAction()
    {
        $cartItems = $this->cart->getItems();
        $cartTotal = $this->cart->getDiscountTotal();
        $discount = $this->cart->getAppliedDiscount();

        $em = $this->getDoctrine()->getManager();
        $currentUser = $em->getRepository(User::class)->findOneBy([]); // current user id needs to be set after sign up

        $order = new Order();

        try {
            $order->setUser($currentUser);
            $order->setTotal($cartTotal);
            $order->setCreatedAt(new \DateTime());
            $em->persist($order);
            $em->flush();

            foreach ($this->cart->getItems() as $item) {

                $orderBook = new OrderBook();
                $orderBook->setQty($item->getQuantity());
                $orderBook->setOrderId($order->getId());
                $orderBook->setBookId($item->getId());
                $orderBook->setTotal($item->getTotal());
                $em->persist($orderBook);
                $em->flush();
            }

            $this->addFlash('success', 'Checkout completed. Your order will be shipped soon.');
            $this->cart->clear();
        } catch (\Exception $exception) {
            $this->addFlash('danger', 'Error'); // need to log the exception details
        }

        return $this->render('cart/invoice.html.twig', [
            'cart' => $cartItems,
            'total' => $cartTotal,
            'orderId' => $order->getId(),
            'discount'=> $discount,
        ]);
    }
}
