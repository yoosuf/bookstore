# Symfony Book Store

```bash

git clone git@bitbucket.org:yoosuf/bookstore.git


composer install


touch .env.example .env


nano .env

#change the value
DATABASE_URL=mysql://USERNAME:PASSWORD@HOST:PORT/DATABASE_NAME?serverVersion=SERVER_VERSION



# setup tables

php bin/console doctrine:migrations:migrate



# dump deeder data 

php bin/console doctrine:fixtures:load
```


start the server

```bash
symfony serve
```



### CRUD urls 
```bash
localhost:8000/admin/books

localhost:8000/admin/categories
```







 
