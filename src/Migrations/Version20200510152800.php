<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200510152800 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_books DROP FOREIGN KEY FK_C9855E6FFCDAEAAA');
        $this->addSql('DROP INDEX IDX_C9855E6FFCDAEAAA ON order_books');
        $this->addSql('ALTER TABLE order_books CHANGE order_id_id order_id INT NOT NULL');
        $this->addSql('ALTER TABLE order_books ADD CONSTRAINT FK_C9855E6F8D9F6D38 FOREIGN KEY (order_id) REFERENCES `orders` (id)');
        $this->addSql('CREATE INDEX IDX_C9855E6F8D9F6D38 ON order_books (order_id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `order_books` DROP FOREIGN KEY FK_C9855E6F8D9F6D38');
        $this->addSql('DROP INDEX IDX_C9855E6F8D9F6D38 ON `order_books`');
        $this->addSql('ALTER TABLE `order_books` CHANGE order_id order_id_id INT NOT NULL');
        $this->addSql('ALTER TABLE `order_books` ADD CONSTRAINT FK_C9855E6FFCDAEAAA FOREIGN KEY (order_id_id) REFERENCES orders (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_C9855E6FFCDAEAAA ON `order_books` (order_id_id)');
    }
}
