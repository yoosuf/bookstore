<?php

namespace App\Controller;

use App\Entity\Book;
use App\Entity\Category;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class HomeController extends AbstractController
{
    /**
     * @Route("/", name="home")
     */
    public function index()
    {

        $em = $this->getDoctrine()->getManager();
        $categories = $em->getRepository(Category::class)->findAll();
        $books  = $em->getRepository(Book::class)->findAll();

        return $this->render('home/index.html.twig', get_defined_vars());
    }
}
