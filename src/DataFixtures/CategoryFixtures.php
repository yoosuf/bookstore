<?php

namespace App\DataFixtures;

use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class CategoryFixtures extends Fixture
{

    public function load(ObjectManager $manager)
    {
        $categoriesData = [
            'Children',
            'Fiction',
        ];

        foreach ($categoriesData as $categoryData) {

            $category = new Category();
            $category->setTitle($categoryData);
            $manager->persist($category);
        }

        $manager->flush();
    }
}
