<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200510183416 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE order_books DROP FOREIGN KEY FK_C9855E6F16A2B381');
        $this->addSql('ALTER TABLE order_books DROP FOREIGN KEY FK_C9855E6F8D9F6D38');
        $this->addSql('DROP INDEX IDX_C9855E6F16A2B381 ON order_books');
        $this->addSql('DROP INDEX IDX_C9855E6F8D9F6D38 ON order_books');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE `order_books` ADD CONSTRAINT FK_C9855E6F16A2B381 FOREIGN KEY (book_id) REFERENCES books (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('ALTER TABLE `order_books` ADD CONSTRAINT FK_C9855E6F8D9F6D38 FOREIGN KEY (order_id) REFERENCES orders (id) ON UPDATE NO ACTION ON DELETE NO ACTION');
        $this->addSql('CREATE INDEX IDX_C9855E6F16A2B381 ON `order_books` (book_id)');
        $this->addSql('CREATE INDEX IDX_C9855E6F8D9F6D38 ON `order_books` (order_id)');
    }
}
