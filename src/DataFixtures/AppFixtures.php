<?php

namespace App\DataFixtures;

use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

class AppFixtures extends Fixture implements ContainerAwareInterface
{
    /**
     * @var
     */
    private $container;


    /**
     * @param ContainerInterface|null $container
     */
    public function setContainer(ContainerInterface $container = null)
    {
        $this->container = $container;
    }


    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {

//        $userManager = $this->container->get('fos_user.user_manager');
//
//        $user = $userManager->createUser();
//        $user->setEmail('admin@admin.com');
//        $user->setPlainPassword('password');
//        $manager->persist($user);
//        $manager->flush();




        // $product = new Product();
        // $manager->persist($product);

        $manager->flush();
    }


}
