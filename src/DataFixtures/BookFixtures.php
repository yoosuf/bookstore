<?php

namespace App\DataFixtures;

use App\Entity\Book;
use App\Entity\Category;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Faker\Factory;

class BookFixtures extends Fixture
{

    private $faker;



    public function load(ObjectManager $manager)
    {
        $this->faker = Factory::create();


        $categories = $manager->getRepository(Category::class)->findAll();

        dd($categories);

        for ($i = 1; $i <= 100; $i++ ) {
            $category = $categories[array_rand($categories)];


            $bookTitle = $this->faker->title;
            $bookPrice = $this->faker->randomNumber(4);
            $bookAuthor = 'Yoosuf';

            $book = new Book();
            $book->setTitle($bookTitle);
            $book->setAuthor($bookAuthor);
            $book->setPrice($bookPrice);
            $book->setCategory($category);

             $manager->persist($book);



        }


        $manager->flush();
    }
}
